#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: RFQ-010:Vac-VEG-10001
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEG-10001, BOARD_A_SERIAL_NUMBER = 1807161527, BOARD_B_SERIAL_NUMBER = 1812041021, BOARD_C_SERIAL_NUMBER = 1812041005, IPADDR = moxa-vac-rfq-1.tn.esss.lu.se, PORT = 4001")

#
# Device: RFQ-010:Vac-VGP-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-10000, CHANNEL = A1, CONTROLLERNAME = RFQ-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-10000, RELAY = 1, RELAY_DESC = 'Process PLC: Atmospheric pressure'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-10000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: RFQ-010:Vac-VGC-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-10000, CHANNEL = B1, CONTROLLERNAME = RFQ-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-10000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-10000, RELAY = 2, RELAY_DESC = 'Process PLC:'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-10000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-10000, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: RFQ-010:Vac-VGC-40000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-40000, CHANNEL = C1, CONTROLLERNAME = RFQ-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-40000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-40000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-40000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-40000, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: RFQ-010:Vac-VEG-10002
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEG-10002, BOARD_A_SERIAL_NUMBER = 1807121009, BOARD_B_SERIAL_NUMBER = 1609080821, BOARD_C_SERIAL_NUMBER = 1505070814, IPADDR = moxa-vac-rfq-1.tn.esss.lu.se, PORT = 4002")

#
# Device: RFQ-010:Vac-VGC-20000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-20000, CHANNEL = B1, CONTROLLERNAME = RFQ-010:Vac-VEG-10002")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-20000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-20000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-20000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-20000, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: RFQ-010:Vac-VGC-30000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-30000, CHANNEL = C1, CONTROLLERNAME = RFQ-010:Vac-VEG-10002")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-30000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-30000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-30000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGC-30000, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: RFQ-010:Vac-VEG-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = RFQ-010:Vac-VEG-01100, BOARD_A_SERIAL_NUMBER = 1807121113, BOARD_B_SERIAL_NUMBER = 1505070713, BOARD_C_SERIAL_NUMBER = 1609091039, IPADDR = moxa-vac-rfq-1.tn.esss.lu.se, PORT = 4003")

#
# Device: RFQ-010:Vac-VGP-05100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-05100, CHANNEL = A1, CONTROLLERNAME = RFQ-010:Vac-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-05100, RELAY = 1, RELAY_DESC = 'Process PLC: Manifold under vacuum'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-05100, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: RFQ-010:Vac-VGP-06100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-06100, CHANNEL = A2, CONTROLLERNAME = RFQ-010:Vac-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-06100, RELAY = 1, RELAY_DESC = 'Process PLC: Manifold under vacuum'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = RFQ-010:Vac-VGP-06100, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")
