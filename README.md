# IOC for RFQ vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   RFQ-010:Vac-VEG-10001
    *   RFQ-010:Vac-VGP-10000
    *   RFQ-010:Vac-VGC-10000
    *   RFQ-010:Vac-VGC-40000
*   RFQ-010:Vac-VEG-10002
    *   RFQ-010:Vac-VGC-20000
    *   RFQ-010:Vac-VGC-30000
*   RFQ-010:Vac-VEG-01100
    *   RFQ-010:Vac-VGP-05100
    *   RFQ-010:Vac-VGP-06100
